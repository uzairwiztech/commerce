package com.commerce.commerce.repository;

import com.commerce.commerce.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {

    Category findByCategoryId(Long categoryId);
}
