package com.commerce.commerce.repository;

import com.commerce.commerce.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantRepository extends JpaRepository<Merchant,Long> {

    Merchant findByMerchantId(Long id);
}
