package com.commerce.commerce.repository;

import com.commerce.commerce.model.AppLog;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoggingRepository extends JpaRepository<AppLog, Long> {
}
