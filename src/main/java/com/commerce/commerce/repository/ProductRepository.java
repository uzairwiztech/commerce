package com.commerce.commerce.repository;

import com.commerce.commerce.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Long> {

    Product findByProductId(Long productId);
    List<Product> findByTitleLike(String title);
    Product findByTitle(String title);
}
