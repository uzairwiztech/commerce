package com.commerce.commerce.service;

import com.commerce.commerce.model.AppLog;
import com.commerce.commerce.model.Category;
import com.commerce.commerce.repository.CategoryRepository;
import com.commerce.commerce.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class CategoryServiceImpl implements CategoryService{

    private static final Logger LOGGER = Logger.getLogger(CategoryServiceImpl.class.getName());

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private LoggingService loggingService;

    @Override
    public Category saveCategory(Category category) {
        if(category.getCategoryId() != null){
            loggingService.insertLog(new AppLog(Constants.UPDATE,"Category : " + category.getTitle() + " updated"));
        }else{
            loggingService.insertLog(new AppLog(Constants.INSERT,"Category : " + category.getTitle() + " added"));
        }
        return categoryRepository.save(category);
    }

    @Override
    public Category getById(Long categoryId) {
        return categoryRepository.findByCategoryId(categoryId);
    }

    @Override
    public List<Category> getAll() {
        LOGGER.info("Get All Categories");
        return categoryRepository.findAll();
    }

    @Override
    public boolean deactivateCategory(Long categoryId) {
        Category category = categoryRepository.findByCategoryId(categoryId);
        if(null != category ){
            category.setActive(false);
            categoryRepository.save(category);
            loggingService.insertLog(new AppLog(Constants.DELETE,"Category : " + category.getTitle() + " deactivated"));
            return true;
        }
        return false;
    }
}
