package com.commerce.commerce.service;

import com.commerce.commerce.model.AppLog;
import com.commerce.commerce.model.Product;
import com.commerce.commerce.repository.ProductRepository;
import com.commerce.commerce.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class ProductServiceImpl implements ProductService {

    private static final Logger LOGGER = Logger.getLogger(ProductServiceImpl.class.getName());

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private LoggingService loggingService;

    @Override
    public Product save(Product product) {
        if(product.getProductId() != null){
            loggingService.insertLog(new AppLog(Constants.UPDATE,"Product : " + product.getTitle() + " updated"));
        }else{
            loggingService.insertLog(new AppLog(Constants.INSERT,"Product : " + product.getTitle() + " added"));
        }
        return productRepository.save(product);
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findByProductId(id);
    }

    @Override
    public List<Product> findByTitle(String title) {
        return productRepository.findByTitleLike("%" + title + "%");
    }

    @Override
    public List<Product> getAll() {
        LOGGER.info("Get All Products");
        return productRepository.findAll();
    }

    @Override
    public boolean removeProduct(Long id) {
        Product product = productRepository.findByProductId(id);
        if(null != product ){
            product.setAvailable(false);
            productRepository.save(product);
            loggingService.insertLog(new AppLog(Constants.DELETE,"Product : " + product.getTitle() + " unavailable"));
            return true;
        }
        return false;
    }
}
