package com.commerce.commerce.service;

import com.commerce.commerce.model.Category;

import java.util.List;

public interface CategoryService {

    /**
     * Works for save and update
     * @param category
     * @return
     */
    Category saveCategory(Category category);

    /**
     * Get Category by Id
     * @param categoryId
     * @return
     */
    Category getById(Long categoryId);

    List<Category> getAll();

    /**
     * Deactivate Category
     * @param categoryId
     */
    boolean deactivateCategory(Long categoryId);
}
