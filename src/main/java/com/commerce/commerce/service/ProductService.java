package com.commerce.commerce.service;

import com.commerce.commerce.model.Product;

import java.util.List;

public interface ProductService {

    /**
     * Save or update a product
     * @param product
     * @return
     */
    Product save(Product product);

    /**
     * Get Product by id
     * @param id
     * @return
     */
    Product findById(Long id);

    /**
     * get product by title
      * @param title
     * @return
     */
    List<Product> findByTitle(String title);

    /**
     * Get All products
     * @return
     */
    List<Product> getAll();

    /**
     * remove a product
     * @param id
     */
    boolean removeProduct(Long id);
}
