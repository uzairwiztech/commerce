package com.commerce.commerce.service;

import com.commerce.commerce.model.Merchant;

import java.util.List;

public interface MerchantService {
    /**
     * Works for save and update
     * @param merchant
     * @return
     */
    Merchant saveMerchant(Merchant merchant);

    /**
     * Get Merchant by Id
     * @param merchantId
     * @return
     */
    Merchant getById(Long merchantId);

    List<Merchant> getAll();

    /**
     * Deactivate Merchant
     * @param merchantId
     */
    boolean deactivateMerchant(Long merchantId);
}
