package com.commerce.commerce.service;

import com.commerce.commerce.model.AppLog;
import com.commerce.commerce.repository.LoggingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class LoggingServiceImpl implements LoggingService{

    private static final Logger LOGGER = Logger.getLogger(LoggingServiceImpl.class.getName());

    @Autowired
    private LoggingRepository loggingRepository;

    @Override
    public void insertLog(AppLog appLog) {
        loggingRepository.save(appLog);
    }

    @Override
    public List<AppLog> getAll() {
        LOGGER.info("Get All Logs");
        return loggingRepository.findAll();
    }
}
