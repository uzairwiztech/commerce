package com.commerce.commerce.service;

import com.commerce.commerce.model.AppLog;
import com.commerce.commerce.model.Merchant;
import com.commerce.commerce.repository.MerchantRepository;
import com.commerce.commerce.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class MerchantServiceImpl implements MerchantService{


    private static final Logger LOGGER = Logger.getLogger(CategoryServiceImpl.class.getName());

    @Autowired
    private MerchantRepository merchantRepository;

    @Autowired
    private LoggingService loggingService;

    @Override
    public Merchant saveMerchant(Merchant merchant) {
        if(merchant.getMerchantId() != null){
            loggingService.insertLog(new AppLog(Constants.UPDATE,"Merchant : " + merchant.getTitle() + " updated"));
        }else{
            loggingService.insertLog(new AppLog(Constants.INSERT,"Merchant : " + merchant.getTitle() + " added"));
        }
        return merchantRepository.save(merchant);
    }

    @Override
    public Merchant getById(Long merchantId) {
        return merchantRepository.findByMerchantId(merchantId);
    }

    @Override
    public List<Merchant> getAll() {
        LOGGER.info("Get All Merchants");
        return merchantRepository.findAll();
    }

    @Override
    public boolean deactivateMerchant(Long merchantId) {
        Merchant merchant = merchantRepository.findByMerchantId(merchantId);
        if(null != merchant ){
            merchant.setActive(false);
            merchantRepository.save(merchant);
            loggingService.insertLog(new AppLog(Constants.DELETE,"Merchant : " + merchant.getTitle() + " deactivated"));
            return true;
        }
        return false;
    }
}
