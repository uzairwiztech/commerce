package com.commerce.commerce.service;

import com.commerce.commerce.model.AppLog;

import java.util.List;

public interface LoggingService {

    /**
     * Insert Log
     * @param appLog
     */
    void insertLog(AppLog appLog);

    /**
     * Get All logs
     * @return
     */
    List<AppLog> getAll();


}
