package com.commerce.commerce.model;


import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "app_log")
public class AppLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "log_id")
    private long logId;
    @Column(name = "log_type")
    private String type;
    @Column(name = "timestamp")
    private LocalDateTime timestamp;
    @Column(name = "message")
    private String message;

    public AppLog(String type,String message) {
        this.type = type;
        this.timestamp = LocalDateTime.now();
        this.message = message;
    }

    public long getLogId() {
        return logId;
    }

    public void setLogId(long logId) {
        this.logId = logId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
