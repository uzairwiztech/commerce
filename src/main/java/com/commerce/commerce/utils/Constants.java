package com.commerce.commerce.utils;

public interface Constants {

    public static final String INSERT = "INSERT";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";
    public static final String ERROR = "ERROR";
    public static final String WARN = "WARNING";

}
