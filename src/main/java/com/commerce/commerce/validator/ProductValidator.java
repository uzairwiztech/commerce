package com.commerce.commerce.validator;

import com.commerce.commerce.model.Product;
import com.commerce.commerce.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.ArrayList;
import java.util.List;

/**
 * Custom Product Validator
 */
@Component
public class ProductValidator implements Validator {

    @Autowired
    private ProductRepository productRepository;

    public static List<String> getMessage(Product product){
        List<String> errorList = new ArrayList<>();
        if(product.getTitle() == null || product.getTitle().isEmpty()){
            errorList.add("Title is required");
        }
        if(product.getPrice() == null){
            errorList.add("Price is required");
        }
        if(product.getTitle() == null || product.getTitle().isEmpty()){
            errorList.add("Title is required");
        }
        if(product.getTitle() == null || product.getTitle().isEmpty()){
            errorList.add("Title is required");
        }
        return errorList;
    }


    @Override
    public boolean supports(Class<?> aClass) {
        return Product.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Product product = (Product) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "available", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "categoryId", "NotEmpty");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "merchantId", "NotEmpty");
        if (productRepository.findByTitle(product.getTitle())!= null) {
            errors.rejectValue("title", "Duplicate.product");
        }

    }
}
