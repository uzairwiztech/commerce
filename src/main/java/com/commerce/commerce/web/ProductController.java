package com.commerce.commerce.web;


import com.commerce.commerce.model.Category;
import com.commerce.commerce.model.Merchant;
import com.commerce.commerce.model.Product;
import com.commerce.commerce.service.CategoryService;
import com.commerce.commerce.service.MerchantService;
import com.commerce.commerce.service.ProductService;
import com.commerce.commerce.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private ProductValidator productValidator;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private MerchantService merchantService;

    @RequestMapping(value = "/add" ,method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> add(@RequestBody  Product product , BindingResult bindingResult) {
        productValidator.validate(product, bindingResult);
        if(bindingResult.hasErrors()){
            /*StringBuilder stringBuilder = new StringBuilder();
            bindingResult.getAllErrors().forEach(e -> stringBuilder.append(e.getDefaultMessage()));*/
            return new ResponseEntity<>(
                    bindingResult.getAllErrors().toString(),
                    HttpStatus.BAD_REQUEST);
        }
        Category category = categoryService.getById(product.getCategoryId()); // not a part of validator to avoid multiple db hits
        if(category == null){
            return new ResponseEntity<>(
                    "Category Not found",
                    HttpStatus.NOT_FOUND);
        }
        Merchant merchant = merchantService.getById(product.getMerchantId()); // not a part of validator to avoid multiple db hits
        if(merchant == null){
            return new ResponseEntity<>(
                    "Merchant Not found",
                    HttpStatus.NOT_FOUND);
        }
        product.setCategory(category);
        product.setMerchant(merchant);
        productService.save(product);
        return new ResponseEntity<>(
                "Product Added Successfully",
                HttpStatus.OK);
    }


    @GetMapping(value = "/get")
    public ResponseEntity<List<Product>> getByTitle(@RequestParam(value = "search", defaultValue = "") String title) {

        List<Product> productList = productService.findByTitle(title);
        if(productList != null) {
            return new ResponseEntity<>(
                    productList,
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                null,
                HttpStatus.NOT_FOUND);

    }

    @GetMapping(value = "/getAll")
    public ResponseEntity<List<Product>> getAll() {

        return new ResponseEntity<>(
                productService.getAll(),
                HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody Product product) {
        Category category = categoryService.getById(product.getCategoryId());
        Merchant merchant = merchantService.getById(product.getMerchantId());
        if( category!= null && merchant != null && productService.findById(id) != null) {
            product.setCategory(category);
            product.setMerchant(merchant);
            productService.save(product);
            return new ResponseEntity<>(
                    "Product updated Successfully",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Category, Merchant or Product not found",
                HttpStatus.NOT_FOUND);

    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id) {


        if(productService.removeProduct(id)) {
            return new ResponseEntity<>(
                    "Product set to unavailable",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Product Not found",
                HttpStatus.NOT_FOUND);

    }

}
