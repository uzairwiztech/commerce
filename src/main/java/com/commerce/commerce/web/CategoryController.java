package com.commerce.commerce.web;

import com.commerce.commerce.model.Category;
import com.commerce.commerce.model.Product;
import com.commerce.commerce.service.CategoryService;
import com.commerce.commerce.service.ProductService;
import com.commerce.commerce.validator.ProductValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "/add" ,method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> add(@RequestBody Category category, BindingResult bindingResult) {

        categoryService.saveCategory(category);
        return new ResponseEntity<>(
                "Category Added Successfully",
                HttpStatus.OK);
    }


    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Category> getById(@PathVariable("id") Long id) {

        Category category = categoryService.getById(id);
        if(category != null) {
            return new ResponseEntity<>(
                    category,
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                null,
                HttpStatus.NOT_FOUND);

    }

    @GetMapping(value = "/get")
    public ResponseEntity<List<Category>> getAll() {

            return new ResponseEntity<>(
                    categoryService.getAll(),
                    HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody Category category) {

        if(categoryService.getById(id) != null) {
            category.setCategoryId(id); // jpa will call update if id is set
            categoryService.saveCategory(category);
            return new ResponseEntity<>(
                    "Category updated Successfully",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Category not found",
                HttpStatus.NOT_FOUND);

    }

    @DeleteMapping(value = "/deactivate/{id}")
    public ResponseEntity<String> deactivate(@PathVariable("id") Long id) {


        if(categoryService.deactivateCategory(id)) {
            return new ResponseEntity<>(
                    "Category Deactivated",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Category Not found",
                HttpStatus.NOT_FOUND);

    }
}
