package com.commerce.commerce.web;



import com.commerce.commerce.model.Merchant;
import com.commerce.commerce.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/merchant")
public class MerchantController {

    @Autowired
    private MerchantService merchantService;

    @RequestMapping(value = "/add" ,method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> add(@RequestBody Merchant merchant, BindingResult bindingResult) {

        merchantService.saveMerchant(merchant);
        return new ResponseEntity<>(
                "Merchant Added Successfully",
                HttpStatus.OK);
    }


    @GetMapping(value = "/get/{id}")
    public ResponseEntity<Merchant> getById(@PathVariable("id") Long id) {

        Merchant merchant = merchantService.getById(id);
        if(merchant != null) {
            return new ResponseEntity<>(
                    merchant,
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                null,
                HttpStatus.NOT_FOUND);

    }

    @GetMapping(value = "/get")
    public ResponseEntity<List<Merchant>> getAll() {

        return new ResponseEntity<>(
                merchantService.getAll(),
                HttpStatus.OK);
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity<String> update(@PathVariable("id") Long id, @RequestBody Merchant merchant) {

        if(merchantService.getById(id) != null) {
            merchant.setMerchantId(id); // jpa will call update if id is set
            merchantService.saveMerchant(merchant);
            return new ResponseEntity<>(
                    "Merchant updated Successfully",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Merchant not found",
                HttpStatus.NOT_FOUND);

    }

    @DeleteMapping(value = "/deactivate/{id}")
    public ResponseEntity<String> deactivate(@PathVariable("id") Long id) {


        if(merchantService.deactivateMerchant(id)) {
            return new ResponseEntity<>(
                    "Merchant Deactivated",
                    HttpStatus.OK);
        }
        return new ResponseEntity<>(
                "Merchant Not found",
                HttpStatus.NOT_FOUND);

    }
}
